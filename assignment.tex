\documentclass[a4paper,twocolumn]{scrartcl}

\usepackage[utf8]{inputenc}
\usepackage{csquotes}
\usepackage[english]{babel}
\usepackage{hyperref}
\usepackage[
backend=biber,
]{biblatex}
\usepackage{csquotes}

\addbibresource{assignment.bib}

\title{Man, Machine: A Mathematical Approach}
\subtitle{Philosophy and Ethics}
\author{Erin van der Veen}

\begin{document}

\maketitle

\begin{abstract}

\end{abstract}

\section*{Introduction}
In his famous 1950 paper \enquote{Computing Machinery and Intelligence},
Turing introduced the idea of categorizing AI (Artificial Intelligence) based on their interaction with a human.
If the human was not able to,
	purely by textual interaction,
	determine if he or she was talking to an AI or human.
Then the AI would have passed the \enquote{Imitation Game}.

Turing suggested that this this test could be used to determine \enquote{Can Machines Think?}\cite{turing1950computing}.
He never suggested that his \enquote{Imitation Game} could be used to differentiate computer from humans.
And yet, this is what we often see.
It makes, of course, complete sense to do this.
If a human cannot distinguish an AI from a human, it must be a human.
In the very famous duck test: \enquote{If it types like a human, responds like a human, and asks questions like a human, then it probably is a human}.
But, one should beware the duck test~\cite{jentleson2011beware}.

Several years before Turing coined the \enquote{Imitation Game},
	he coined the term \enquote{Computing Machine}~\cite{turing1937computable}
With this, came the term \enquote{Turing Completeness}.
Turing complete machines are those who, in finite time, can calculate exactly the same problems as Turing's Computing Machines.

If we consider the brain to be a machine,
	we can extend this concept to the human brain.
If the human brain is not Turing complete,
	then we know that (Turing complete) machines are more powerful than the human brain.
Is the human brain Turing complete,
	then we can at least say that machines are not more powerful than humans.
The last possibility is that the human brain is at least Turing complete.
If this is the case, the human brain is more powerful than (Turing complete) machines.

There are many philosophies behind the difference of man machine,
	in this paper we will attempt answer the question:
\enquote{Is the human brain Turing Complete?}
Once we have attempted this,
	we will look at the existing comparisons and their difference to our idea.

\section{Turing Completeness of the Brain}
This section attempts to ask the question if the human brain is Turing complete.

In first instance, the brain is not Turing complete.
The simple argument being that our memory is limited (as opposed to the Turing Machine's infinite tape).
However, humans are known for their use of tools.
If we allow the human to have infinite pencils and infinite stack of paper,
	the human would have access to the infinite tape.
And thus no longer suffer from this problem.
It is important to put on a limit on the tools we allow the human brain to use.
If we allow humans to use all tools they have created,
	they will have access to a computer.
Meaning that the comparison in itself would be less useful.

This in itself, however, does not proof that a human is Turing complete.
In order to proof such a thing,
mathematicians (and computing scientists) typically use a formal proof in the form of a reduction.
In order to proof Turing completeness via reduction,
	we must show that the human brain is capable of emulating a known Turing complete machine.
Most, humans, when taught how a Turing Machine works,
	are very capable of simulating their behaviour using basic tools.
Since this enables us to simulate a Turing Machine,
	we must at least be Turing Complete.

\section{More than Turing Complete}
It is technically possible for machines to be more than Turing complete.
For this to be true, we must create a program/computation that can be executed/performed by the human brain,
	but not by Turing's \enquote{Computing Machines}.

\subsection{Randomness}
Lets suppose for a moment that the human brain is able to generate true random numbers.
While pseudo random number generators exist,
	generating true random numbers is something computers have always struggled with.
A simple solution might be to calculate a number like $pi$, $e$ or $\sqrt{2}$,
	and use their infinite sequence as random numbers.
This is unfortunately not random, as it is 100\% predictable.

The way modern computer deal with generating random numbers is through their interaction with the outside world.
This allows them to use the inherent randomness of the real world to generate these numbers.
Consequently, they are more than Turing Complete.
In fact, they belong in the same class as the Probabilistic Turing Machine~\footnote{Note, this is not the same as a Non-Deterministic Turing Machine. Non-Deterministic Turing Machines can calculate everything a normal Turing Machine can}.
If the assumption we made at the beginning of this subsection holds,
	humans must also be part of this same class.

This leaves us in a rather peculiar situation,
	through our assumption we have shown that humans are in fact capable of computing more than the Turing Complete class of machines.
But we have also shown that modern computers can,
	though interaction with the outside world,
	achieve the same thing.
If we assume that randomness is the only thing that differentiates the human brain from Turing Machine,
	we must reach the conclusion that the human brain is not more powerful than a modern computer.

Andrew S. Friedman comes to a different conclusion when looking at the same problem in his 2002 article~\cite{friedman2002fundamental}.
Specifically, Friedman comes to the conclusion that genetic algorithms are required for machines to generate true random numbers,
	and states that a Turing Machine including a genetic algorithm is no longer a Turing Machine.
While his argument is correct, he neglects the fact that there are different classes of Turing Machines exactly for this purpose.
Adding randomness to a Turing Machine would allow it to simulate genetic algorithms.

\subsection{Minds, Machines and Gödel~\cite{lucas1961minds}}
Friedman goes on to derive an argument based on John Lucas' 1961 paper.
In this paper, Lucas uses Gödel's incompleteness theorem to show that the human mind can not be represented by a logical system.

First, recall Gödel's first incompleteness theorem:
\begin{displayquote}
	Any consistent formal system $F$ within which a certain amount of elementary arithmetic can be carried out is incomplete; i.e., there are statements of the language of $F$ which can neither be proved nor disproved in $F$.~\cite{raatikainen2015goedel}
\end{displayquote}

The essence of Lucas' argument rests on this incompleteness theorem.
He reasons that, since a human is capable of creating a formula that cannot be proven or disproven in a Turing Machine,
	we must be more than Turing Complete.

Friedman recognizes a potential issue and brings to light a counter argument raised by Benacerraf.
The essence of this argument is that Gödel's theorems no longer hold up if we allow the Machines to modify themselves.
He uses this critique as an argument for the fact that genetic algorithms are needed to support conciousness.

\subsection{Randomness or Genetic Algorithms}
In this section we have looked at two ways one can show that the human conciousness is not computable.
In the argument described in this paper,
	we conclude that randomness is required for computing machines to perform tasks that the human brain can do.
Friedman comes to the conclusion that genetic algorithms are required to perform these same tasks.
Through mathematics, he also shows that genetic algorithms cannot be simulated without having access to true randomness.

It shows then, that true randomness and genetic algorithms are,
	in the context of computability,
	the same.
Unfortunately, this does not lead to a satisfying conclusion.
The human mind is not calculable,
	but given access to randomness,
	can be simulated.

\section{Probabilistic Turing Machines}
A Theorem due Sacks~\cite{sacks1964recursively} shows that any problem computable on Probabilistic Turing Machines is also computable on normal Turing Machines.
Yet, in the previous section we have shown exactly such a problem.
This apparent inconsistency lies in reproducibility~\footnote{The idea for this differentiation comes from a Stack Exchange question}\footnote{https://cstheory.stackexchange.com/questions/1263/truly-random-number-generator-turing-computable\label{cstheory}}.
Each and every time a probabilistic Turing Machine performs a computation, the results will be different.
Instead, we should formulate a question that is based on the acceptance criteria of a probabilistic Turing Machine:
\begin{displayquote}
	is there a function $f$ which is non-computable,
	but which can be "computed with positive probability",
	in the sense that there is an Turing machine with access to a random oracle which,
	with positive probability (over the oracle), computes $f$.~\addtocounter{footnote}{-1}\addtocounter{Hfootnote}{-1}\footnotemark
\end{displayquote}
Sacks' theorem proves exactly that such a function does not exist.

Note, however, that this does not mean that there are no problems a probabilistic Turing Machine can solve, that a normal Turing Machine cannot.
Rather, it shows that there exists no problem~\cite{turing1950computing} with a single answer that can be solved by a probabilistic Turing Machine (with a positive probability),
	that cannot be solved by a normal Turing Machine.
This means that the probabilistic Turing Machine is still more powerful in the case where the answer to the problem is strictly random.

Consequently, the human brain is not actually more than Turing Complete.

\section{The Mind as a Probabilistic Turing Machine}
In the previous section we looked at whether the mind was Turing Complete,
	and whether the mind was just as powerful as a probabilistic Turing Machine.
The short term conclusion being that it was Turing Complete,
	and that it seems to be equivalent to a probabilistic Turing Machine.
In this section we will look at how this analysis compares to the Chinese Room experiment.
Additionally, we will attempt to place it in the Inconsistent Tetrad.
For the sake of analysis, in this we assume the mind is exactly a probabilistic Turing Machine.

\subsection{The Chinese Room}
Searle's Chinese Room experiment seems to support his idea that the human brain is capable of somethings that a machine is incapable of.
In particular he references \enquote{understanding} as something a computer could never do.
Basically, he states that, regardless of how a system behaves, it will never be able to \enquote{understand}.

His argument is completely opposite to the definitions used in computing science.
If we extend this conclusion to computability theory, we can say that, despite Turing Machines being just as powerful as the $\lambda$-calculus~\cite{turing1937computable},
	since their essence of calculation is different, they are distinct.
The difference in the Chinese room being the \enquote{understanding}.

In mathematics,
	before we determine if two things are equivalent,
	we must first define the equality relation.
When talking about computing frameworks $A$ and $B$,
	we typically state that $A = B$ if and only if there exists no problem a that can be computed by $A$ but not by $B$ or by $B$ but not by $A$.
Alternatively:
	for any problem $p$ it holds that $A$ can compute $p$ if and only if $B$ can compute $p$.
Let it be obvious that, given this relation,
the human mind and the probabilistic Turing machine are the same~\footnote{If the assumption from the beginning of this section holds.}.

Defining a good alternative relation in which the mind is not equal to the probabilistic Turing Machine is not easy.
Because we quickly run into the problem where we assume that two things can only be equivalent if they have the same inner workings.
Generalizing this scheme makes it look something like this:
\begin{enumerate}
	\item Assume $A \neq B$
	\item Then $A \neq B$
\end{enumerate}
Which is obviously not a very convincing proof.

\subsubsection{A Possible Relation}
Can we create a relation that does not suffer from the above scheme?
In order to create a compatible equivalence relation,
	is must somehow incorporate the \enquote{understanding} that Searle mentions in his experiment.
Let's interpret \enquote{understanding} as being the specific way in which humans maintain memories,
	i.e. the internal state of the brain.
We then come to the following definition:

\paragraph{Definition} Computing frameworks $A$ and $B$ are equivalent if and only if their internal representations of a partial solution $s$ are equivalent.

It does suffer slightly from the problem we looked at a little earlier;
	we create our definition to satisfy the problem.
On the other hand, it does create a more general definition to Searle's \enquote{understanding}.

The above definition is compatible with Searle's findings.
The Turing Machine and the human brain are no longer equivalent~\footnote{When we assume that the human brain does not have an infinite tape with 0's and 1's.}.
Unfortunately, the definition also concludes that the Turing Machine and the $\lambda$-calculus are no longer equivalent.
With the $\lambda$-calculus being completely state free.
Weirdly enough, the Normal Turing Machine and its probabilistic variant are still equivalent,
	both using the infinite tape of 0's and 1's.

\subsection{The Inconsistent Tetrad~\cite{ludwig2003mind}}
Using the assumption made above, we will look at how our theory fits in the Inconsistent Tetrad.
Ideally, this will show that the theory is compatible with an already existing theory.

When we reduce the mind to a probabilistic Turing Machine,
	we consider every kind behavior produced by the brain to be a probabilistic computation.
It is nearly impossible to defend the statement that a probabilistic computation is somehow mental.
Therefore, we must reject the statement that there somehow exist any kind of \enquote{mental}.
Consequently, our theory falls into the same category as \enquote{Eliminativist Materialism}.

However, if we do not assume that the mind is a probabilistic Turing Machine,
	we have only shown that the human brain is just as powerful.
This says nothing about the \enquote{mental} aspect of the machine.
In fact, this brings us back to the Chinese room experiment from earlier.
Just because it behaves like a probabilistic Turing Machine from the outside,
	doesn't mean that it is a probabilistic Turing Machine~\footnote{According to Searle}.

\section{Conclusion}
In this paper, we attempted to see how the brain can be compared to traditional computing frameworks.
First we attempted to show that the brain is Turing complete,
	which was quite trivial~\footnote{After we made the assumption that the human has access to an infinite supply of pens and paper}.
Once we had proven this, we considered the possibility that the human brain might actually be able to calculate problems that the traditional Turing Machine cannot.
We came to the conclusion that the generation of random numbers is such a problem.
Suggesting that our mind is equivalent to probabilistic Turing Machines
	since they are both Turing complete, and can generate random numbers.
Unfortunately, we also deduced that the only extra power such a Turing machine has,
	is that it can generate randomness.
It cannot actually solve any problem that a Turing Machine cannot~\footnote{With a certain probability}.
Under the assumption that modern computers are equivalent to Probabilistic Turing Machines,
	this implies that the human brain is not more powerful than a modern computer.
The conclusion is very weak, however.
Any problem found that can indeed be solved by a human brain,
	but not by a probabilistic Turing machine, completely negates the conclusion.

We then assumed that the brain was exactly a probabilistic Turing Machine and looked at how this idea would compare to existing theories.
We started out by applying the Chinese Room experiment on our theory,
	and noticed that it was relatively hard to show, mathematically, that the Chinese Room experiment proofs any difference between the human brain and the room itself.
Subsequently, we looked at Ludwig's tetrad
	and identified the theses we must reject in order for our Turing Machine to be consistent with the remaining triad.

In general, we have shown that reasoning about the brain in terms of computability provides a good and exact framework.
In particular, this way of reasoning reduces the mind body problem to finding a problem that can only be solved by the brain (not by a Turing machine).
This was attempted by Lucas, although his problem statement is not exact to the point where it provides a conclusive argument.

\section{Further Work}
Lucas provided an argument that humans are more than Turing Complete,
	using Gödels incompleteness theorems.
Friedman subsequently stated that his arguments no longer hold when allowing self modifying Turing Machines.
Additionally, we have argued that a probabilistic Turing machine would allow genetic algorithms.
Thus implying that Lucas' argument would not holds for probabilistic Turing machines.
However, Sacks showed that probabilistic Turing Machines are not more powerful than normal Turing Machines.
The statements above create an inconsistency,
	meaning that one of the arguments is incorrect.

In further work, it would be interesting to determine exactly where this inconsistency lies.

\printbibliography

\section{Closing Notes}
There are many ideas that did not fit in the paper.
This section serves as a small collection of these ideas.

\subsection*{Randomness and Genetic Algorithms}
Friedman provides a sound proof to show that Turing Machines can not ever generate true randomness.
This leads to the conclusion that genetic algorithms could not ever be performed on a Turing machine.
While this is true, I do wonder how this relates to genetic algorithms in a practical setting.
In most cases, computers do not use true randomness in cases where it is not strictly needed.
This includes in genetic algorithms.
For such cases, they instead tend to rely on pseudo random numbers.
What are the consequences of this on genetic algorithms?
Should we create different classes of genetic algorithms that accurately encompass their power?

\subsection*{Gödel, Lucas, and Penrose}
In Friedman's paper, he refers to an argument by Penrose.
As far as I am able to tell Penrose wrote a review on Lucas' argument,
	and is generally considered to be stronger.
Penrose seems to reduce conciousness to the inherent randomness of quantum physics.
Arguing that synapses are in some way influenced by this randomness.
I can not pretend to know enough about biology nor quantum physics to fully understand his reasoning,
	although I think I would like to.
In a sense, Penrose arrives at the same conclusion I arrive at.
Humans and computers are only as random as the world they live in.

\subsection*{Sacks' Theorem}
I have found quite a lot of information on Sacks' Theorem,
and that it does indeed proof that probabilistic Turing Machines are (in a way) not more powerful than the original Turing Machine.
Ideally, I would like to add a section explaining how it proofs this.
Unfortunately, I need to completely understand the theorem myself before I can adequately describe it.
I don't have the time currently to look into this.

\subsection*{Scope}
I set out this paper looking at how the human brain fits into computability theory,
	in an effort to determine if a human is just a computation machine.
However, I slowly realized that this was a little too hard to create a paper about.
So instead, I started considering more and more things.
Ideally, I would have liked to get to know the theories on the brain as a computing machine a little more,
	and focus on that.

\end{document}
